package casper.render.babylon

import casper.collection.map.IntMap2d
import casper.render.material.Sampling

class TextureInfo(val name: String, val map: IntMap2d, val sampling: Sampling)