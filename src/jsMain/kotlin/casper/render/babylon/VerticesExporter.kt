package casper.render.babylon

import BABYLON.Geometry
import BABYLON.VertexBuffer
import BABYLON.VertexData
import casper.geometry.Vector3d
import casper.render.extension.VerticesBuilder
import casper.render.vertex.Vertex
import casper.render.vertex.Vertices

internal object VerticesExporter {

	fun fromNative(geometry: Geometry, clockWiseOrientation:Boolean): Vertices {
		val builder = VerticesBuilder()

		val indices = IndicesConverter.getIndices(geometry.getIndices())
		val positions = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.PositionKind))
		val normals = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.NormalKind))
		val colors = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.ColorKind))
		val uvs = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.UVKind))
		val uvs2 = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.UV2Kind))
		val uvs3 = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.UV3Kind))
		val uvs4 = VerticesConverter.getVertices(geometry.getVerticesData(VertexBuffer.UV4Kind))

		val buildVertex: (Int) -> Vertex = { vertexIndex ->
			Vertex(
					FloatArrayUtil.toVector3d(positions, vertexIndex) ?: Vector3d.ZERO,
					FloatArrayUtil.toVector3d(normals, vertexIndex),
					FloatArrayUtil.toVector4d(colors, vertexIndex),
					FloatArrayUtil.toVector2d(uvs, vertexIndex),
					FloatArrayUtil.toVector2d(uvs2, vertexIndex),
					FloatArrayUtil.toVector2d(uvs3, vertexIndex),
					FloatArrayUtil.toVector2d(uvs4, vertexIndex)
			)
		}
		if (indices != null) {
			if (!clockWiseOrientation) {
				(0 until indices.size / 3).forEach {triangleIndex->
					builder.add(buildVertex(indices[triangleIndex * 3 + 2]))
					builder.add(buildVertex(indices[triangleIndex * 3 + 1]))
					builder.add(buildVertex(indices[triangleIndex * 3 + 0]))
				}
			} else {
				(0 until indices.size / 3).forEach {triangleIndex->
					builder.add(buildVertex(indices[triangleIndex * 3 + 0]))
					builder.add(buildVertex(indices[triangleIndex * 3 + 1]))
					builder.add(buildVertex(indices[triangleIndex * 3 + 2]))
				}
			}
//			indices.forEach { vertexIndex ->
//				builder.add(buildVertex(vertexIndex))
//			}

		} else if (positions != null) {
			(0 until positions.size / 3).forEach { vertexIndex ->
				builder.add(buildVertex(vertexIndex))
			}
		}
		return builder.get()
	}


	fun toNative(vertices: Vertices, createIndices: Boolean): VertexData {
		val helper = Helper(vertices, createIndices)

		helper.generate()
		return helper.build()
	}
}
