package casper.render.babylon

import BABYLON.*
import casper.collection.mapping
import casper.geometry.Quaternion
import casper.geometry.Transform
import casper.loader.createBitmapLoader
import casper.render.SceneData
import casper.render.material.MaterialReference
import casper.render.material.Sampling
import casper.render.model.SceneModel
import casper.render.model.SceneNode
import casper.render.model.TimeLine
import casper.render.vertex.VerticesReference
import casper.signal.concrete.EitherPromise
import casper.signal.concrete.EitherSignal
import casper.util.toQuaternion
import casper.util.toVector3d
import kotlin.collections.set
import kotlin.math.roundToInt

class SceneExporter(scene: Scene, val name: String, val originalContainer: AssetContainer) : EitherPromise<SceneData, String> by EitherSignal() {
	private val container = AssetContainerAdapter(originalContainer)
	private val imageMap = mutableMapOf<BaseTexture, TextureInfo>()
	private val nodeMap = mutableMapOf<Node, SceneNode>()
	private var imageLoading = 0

	//	todo:	i dont know how define inverse orientation
	private val clockWiseOrientation = !name.contains(".gltf")

	init {
		container.textureMap.values.forEach { texture -> createTextureLoader(texture) }
		tryBuild()
	}

	private fun createTextureLoader(texture: BaseTexture) {
		if (texture is Texture) {
			val url = texture.url
			if (url != null) {
				imageLoading++
				val realURL = if (url.startsWith("data:")) url.substring(5) else url
				createBitmapLoader(realURL).then({ bitmap ->
					imageMap[texture] = TextureInfo(bitmap.name, bitmap.data, getSampling(texture))
					--imageLoading
					tryBuild()
				}, {
					reject("Can't load image: $it")
				})
			}
		}
	}

	private fun getSampling(texture: BaseTexture): Sampling {
		if (texture is Texture) {
//		todo: i see now sampling not exported
//			println("sampling-mode: ${texture.samplingMode.roundToInt()}")
			return when (texture.samplingMode.roundToInt()) {
				Constants.TEXTURE_NEAREST_NEAREST -> Sampling.NEAREST
				Constants.TEXTURE_NEAREST_NEAREST_MIPNEAREST -> Sampling.NEAREST
				Constants.TEXTURE_NEAREST_NEAREST_MIPLINEAR -> Sampling.NEAREST
				Constants.TEXTURE_LINEAR_NEAREST_MIPNEAREST -> Sampling.NEAREST
				Constants.TEXTURE_LINEAR_NEAREST_MIPLINEAR -> Sampling.NEAREST
				Constants.TEXTURE_LINEAR_NEAREST -> Sampling.NEAREST
				else -> Sampling.LINEAR
			}
		} else {
			return Sampling.LINEAR
		}
	}

	private fun tryBuild() {
		if (imageLoading != 0) return

		try {
			val verticesMap = container.geometries.mapping {
				Pair(it, VerticesReference(VerticesExporter.fromNative(it, clockWiseOrientation), it.id))
			}

			val materialMap = container.materialMap.map {
				Pair(it.value, MaterialReference(MaterialBuilder.fromNative(imageMap, it.value), it.key))
			}.toMap()

			val mainNodes = mutableListOf<SceneNode>()
			val allNodes = mutableListOf<SceneNode>()


			container.nodes.forEach { nativeNode ->
				val node = getOrCreateNode(verticesMap, materialMap, nativeNode)
				if (nativeNode.parent == null) {
					mainNodes.add(node)
				}
				allNodes.add(node)
			}

			val animationGroups = originalContainer.animationGroups.map { groupNative ->
				AnimationExporter.buildAnimationGroup(nodeMap, groupNative)
			}

			val data = SceneData(
					name,
					verticesMap.values.toList(),
					materialMap.values.toList(),
					allNodes.toSet().toList(),
					animationGroups,
					SceneModel(
							children = mainNodes.toMutableSet(),
							name = name
					)
			)
			accept(data)
		} catch (error: Throwable) {
			println(error)
			reject(error.toString())
		}
	}

	private fun getTransform(it: TransformNode): Transform {
		val nativeQuaternion = it.rotationQuaternion
		val rotation: Quaternion = if (nativeQuaternion == null) {
			Quaternion.fromEulerAngles(-it.rotation.toVector3d())
		} else {
			nativeQuaternion.toQuaternion()
		}

		val scale = it.scaling.toVector3d()
		val position = it.position.toVector3d()

		return Transform(position, scale, rotation)
	}


	private fun getOrCreateNode(verticesMap: Map<Geometry, VerticesReference>, materialMap: Map<Material, MaterialReference>, nativeNode: Node): SceneNode {
		val last = nodeMap[nativeNode]
		if (last != null) return last

		val children = nativeNode.getChildren().map {
			getOrCreateNode(verticesMap, materialMap, it)
		}

		val name = nativeNode.name

		var transform: Transform? = null
		var mesh: casper.render.model.Mesh? = null

		if (nativeNode is TransformNode) {
			transform = getTransform(nativeNode)
		}
		if (nativeNode is InstancedMesh) {
			val vertices = verticesMap.get(nativeNode.sourceMesh.geometry)

			val material = materialMap.get(nativeNode.material)
			if (vertices != null && material != null) {
				mesh = casper.render.model.Mesh(vertices, material, nativeNode.name)
			}
		} else if (nativeNode is BABYLON.Mesh) {
			val vertices = verticesMap.get(nativeNode.geometry)
			val material = materialMap.get(nativeNode.material)
			if (vertices != null && material != null) {
				mesh = casper.render.model.Mesh(vertices, material, nativeNode.name)
			}
		}
		val animations = nativeNode.animations.map { AnimationExporter.getAnimations(it) }
		val content = SceneModel(mesh = mesh, name = name, children = children.toMutableSet())
		val node = SceneNode(transform ?: Transform.IDENTITY, content, TimeLine(animations = animations))
		nodeMap[nativeNode] = node

		return node
	}

}