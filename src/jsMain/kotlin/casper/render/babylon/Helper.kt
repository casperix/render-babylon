package casper.render.babylon

import BABYLON.VertexData
import casper.render.vertex.Vertex
import org.khronos.webgl.Float32Array
import org.khronos.webgl.Uint32Array
import org.khronos.webgl.set

internal class Helper(val vertices: List<Vertex>, val createIndices: Boolean) {

	private var positions: Float32Array? = null
	private var colors: Float32Array? = null
	private var normals: Float32Array? = null
	private var uvs: Float32Array? = null
	private var uvs2: Float32Array? = null
	private var uvs3: Float32Array? = null
	private var uvs4: Float32Array? = null

	fun generate() {
		for (vertexIndex in 0 until vertices.size) {
			val vertex = vertices[vertexIndex]
			FloatArrayUtil.fromVector3d(vertex.position, vertexIndex) {
				if (positions == null) {
					positions = Float32Array(vertices.size * 3)
				}
				positions!!
			}

			FloatArrayUtil.fromVector4dToColor(vertex.color, vertexIndex) {
				if (colors == null) {
					colors = Float32Array(vertices.size * 4)
				}
				colors!!
			}

			FloatArrayUtil.fromVector3d(vertex.normal, vertexIndex) {
				if (normals == null) {
					normals = Float32Array(vertices.size * 3)
				}
				normals!!
			}

			FloatArrayUtil.fromVector2d(vertex.uvAlbedo, vertexIndex) {
				if (uvs == null) {
					uvs = Float32Array(vertices.size * 2)
				}
				uvs!!
			}

			FloatArrayUtil.fromVector2d(vertex.uvMetallic, vertexIndex) {
				if (uvs2 == null) {
					uvs2 = Float32Array(vertices.size * 2)
				}
				uvs2!!
			}
			//TODO	all other channel
		}
	}

	fun build(): VertexData {

		val vertexData = VertexData()
		positions?.let {
			vertexData.positions = it
		}
		normals?.let {
			vertexData.normals = it
		}
		colors?.let {
			vertexData.colors = it
		}
		uvs?.let {
			vertexData.uvs = it
		}
		uvs2?.let {
			vertexData.uvs2 = it
		}
		uvs3?.let {
			vertexData.uvs3 = it
		}
		uvs4?.let {
			vertexData.uvs4 = it
		}
		if (createIndices) {
			val positions = positions
			if (positions != null) {
				val size = positions.length / 3
				val indices = Uint32Array(size)
				for (index in 0 until size) {
					indices[index] = index
				}
				vertexData.indices = indices
			}
		}
		return vertexData
	}
}