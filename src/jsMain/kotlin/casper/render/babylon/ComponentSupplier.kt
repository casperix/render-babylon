package casper.render.babylon

import BABYLON.BaseTexture
import BABYLON.PushMaterial
import casper.render.material.ColorReference
import casper.render.material.Material
import casper.render.material.MaterialReference
import casper.render.material.TextureReference
import casper.render.model.Mesh
import casper.render.vertex.VerticesReference
import casper.signal.concrete.Signal
import casper.util.forEachTexture

internal class ComponentSupplier(val scene: BABYLON.Scene) {
	private val meshCache = mutableMapOf<Pair<VerticesReference, Material>, BABYLON.Mesh>()
	private val textureToMap = mutableMapOf<BaseTexture, PushMaterial>()
	private val textureCache = mutableMapOf<ColorReference, BABYLON.BaseTexture>()
	private val materialCache = mutableMapOf<Material, BABYLON.PushMaterial>()
	private val vertexListCache = mutableMapOf<VerticesReference, BABYLON.VertexData>()

	val onFrame = Signal<Double>()

	init {
		scene.onAfterRenderObservable.add({ _, _ ->
			val tick = scene.getEngine().getDeltaTime() * 0.001
			onFrame.set(tick)

			textureCache.forEach {
				if (updateTextureTransform(it.value, it.key)) {
					textureToMap[it.value]?.unfreeze()
				}
			}
			meshCache.values.forEach {
				val sourceMesh: dynamic = it
				//	Need, for correct render instanced-mesh ^_^
				sourceMesh._internalAbstractMeshDataInfo._isActive = false
			}
		})
	}

	fun getMesh(model: Mesh): BABYLON.Mesh {
		val key = Pair(model.vertices, model.material.data)
		return meshCache.getOrPut(key) {
			MeshBuilder.createOriginal(this, model)
		}
	}

	fun getGeometry(verticesRef: VerticesReference?): BABYLON.Geometry? {
		val vertices = verticesRef?.data
		if (vertices == null || vertices.isEmpty()) return null
		val vertexData = vertexListCache.getOrPut(verticesRef) {
			VerticesExporter.toNative(vertices, MeshBuilder.USE_INDICES)
		}
		return BABYLON.Geometry("", scene, vertexData)
	}


	fun getMaterial(material: MaterialReference?): BABYLON.Material? {
		if (material == null) return null
		return materialCache.getOrPut(material.data) {
			val data = MaterialBuilder.toNative(scene, material, ::getTexture)
			data.forEachTexture {
				textureToMap[it] = data
			}
			data
		}
	}

	private fun updateTextureTransform(texture: BABYLON.BaseTexture, input: ColorReference): Boolean {
		if (texture !is BABYLON.Texture) return false
		if (input !is TextureReference) return false

		input.transform.matrix?.let { matrix ->
			texture.uOffset = matrix[0, 3]
			texture.vOffset = matrix[1, 3]
			texture.uScale = matrix[0, 0]
			texture.vScale = matrix[1, 1]
			return true
		}
		return false
	}

	fun getTexture(info: ColorReference, channelIndex: Int): BABYLON.BaseTexture {
		return textureCache.getOrPut(info) {
			TextureBuilder.toNative(scene, info, channelIndex)
		}
	}
}

