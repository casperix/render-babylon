package casper.render.babylon

import org.khronos.webgl.*

internal object VerticesConverter {

	fun getVertices(source:Any):Array<Float>? {
		val floats = source as? Array<Number>
		if (floats != null) {
			return floats.map { it.toFloat() }.toTypedArray()
		}

		val float32 = source as? Float32Array
		if (float32 != null) {
			val items = mutableListOf<Float>()
			for (i in 0 until float32.length) {
				items.add(float32.get(i))
			}
			return items.toTypedArray()
		}

		return null
	}
}