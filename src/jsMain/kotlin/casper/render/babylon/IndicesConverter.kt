package casper.render.babylon

import org.khronos.webgl.Int32Array
import org.khronos.webgl.Uint16Array
import org.khronos.webgl.Uint32Array
import org.khronos.webgl.get

internal object IndicesConverter {

	fun getIndices(source:Any?):Array<Int>? {
		val floats = source as? Array<Number>
		if (floats != null) {
			return floats.map { it.toInt() }.toTypedArray()
		}

		val int32 = source as? Int32Array
		if (int32 != null) {
			val items = mutableListOf<Int>()
			for (i in 0 until int32.length) {
				items.add(int32.get(i))
			}
			return items.toTypedArray()
		}

		val uint32 = source as? Uint32Array
		if (uint32 != null) {
			val items = mutableListOf<Int>()
			for (i in 0 until uint32.length) {
				items.add(uint32.get(i))
			}
			return items.toTypedArray()
		}

		val uint16 = source as? Uint16Array
		if (uint16 != null) {
			val items = mutableListOf<Int>()
			for (i in 0 until uint16.length) {
				items.add(uint16.get(i).toInt())
			}
			return items.toTypedArray()
		}

		return null
	}
}