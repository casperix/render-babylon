package casper.util

import BABYLON.Scene
import BABYLON.SceneLoader
import casper.render.SceneData
import casper.render.babylon.SceneExporter
import casper.signal.concrete.EitherFuture
import casper.signal.concrete.EitherSignal


fun createSceneLoader(scene: Scene, url: String): EitherFuture<SceneData, String> {
	val loader = EitherSignal<SceneData, String>()

	SceneLoader.LoadAssetContainer(url, "", scene, {
		try {
			val builder = SceneExporter(scene, url, it)
			builder.then({
				loader.accept(it)
			}, {
				loader.reject(it)
			})
		} catch (error: Throwable) {
			loader.reject("Internal error: $error")
		}
	}, onError = { _, message, exception ->
		loader.reject(message)
	})

	return loader
}
